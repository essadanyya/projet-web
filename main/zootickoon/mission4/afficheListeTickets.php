<?php 
    
    $database = new PDO('mysql:host=localhost;dbname=zootickoon','root','');
    $result = $database->query("SELECT * FROM ticket");
?>
<html>
   <head>
      <meta charset="utf-8">
      <title>listeTickets</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
   </head>
   <body>
      <div class="container">
         <h2>Liste des Tickets</h2>
         <table class="table">
            <thead>
               <tr>
                  <th>id</th>
                  <th>date</th>
                  <th>login</th>
                  <th>sujet</th>
                  <th>description</th>
                  <th>prio</th>
                  <th>secteur</th>
                  <th>statut</th>
               </tr>
               
                <?php 
                    while($row=$result->fetch()){
                        echo "<tr>";
                            echo "<td>".$row['id']."</td>";
                            echo "<td>".$row['datet']."</td>";
                            echo "<td>".$row['login']."</td>";
                            echo "<td>".$row['sujet']."</td>";
                            echo "<td>".$row['description']."</td>";
                            echo "<td>".$row['prio']."</td>";
                            echo "<td>".$row['secteur']."</td>";
                            echo "<td>".$row['statut']."</td>";
                        echo "</tr>";
                    }
                ?>
               
                
            </thead>